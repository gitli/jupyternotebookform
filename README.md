# How to publish your data online

Thank you for participating in this experiment to publish your data online!

1. Click on the 'notebooks.gesis'-link.
2. Wait until it is loaded (this may take a while).
3. You will now see a webpage with a small list of files.
4. Upload your excel (.xlsx) file or your (.csv) file by pressing the 'upload' button in the right side corner.
5. You will now see the file has appeared in the list of files.
6. Open the file 'NotebookForm.ipynb' by clicking on it.
7. You will now see a form consisting of boxes.
8. Make sure 'selection border with left blue stripe' starts in the first box.
9. Press run. The 'selection border with left blue stripe' will now jump to box two.
10. Follow the instructions: once you reach a gray box, adjust the equation (red letters) to your personal project.
11. Do not worry: nothing in the background can be broken, re-running a box is possible at any moment.
12. Keep clicking 'run' for each box after following the instruction, until the end and press 'save'.
13. For now: in the homepage 'list of files' please select the 'NotebookForm.ipynb' (this is now customized to your project), your excel (.xlsx) or (.csv) file and the newly created '.db' file.
14. Please email the 3 files to us.[^1]

If you have questions: please ask us!

[^1]: At some later moment, we will automate the sending of the 3 files, not by email, but directly to the repository it came from.
